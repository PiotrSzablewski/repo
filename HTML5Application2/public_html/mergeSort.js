function mergeSort(arr)
{
    if (arr.length < 2)
        return arr;

    var middle = parseInt(arr.length / 2);
    var left = arr.slice(0, middle);
    var right = arr.slice(middle, arr.length);
    return merge(mergeSort(left), mergeSort(right));

}
function merge(left, right)
{
    var result = [];

    while (left.length && right.length) {
        if (left[0] <= right[0]) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        }
    }


    while (left.length)
        result.push(left.shift());

    while (right.length)
        result.push(right.shift());
    return result;

}

let mergeSortOnload = () => {
    document.getElementById('merge').addEventListener('click', function () {
        let time = yProfiler();
        time.start();
        var wynik = mergeSort(r);
        console.log(wynik);
        let str = '';
        for (let i = 0; i < wynik.length; i++) {
            str += wynik[i] + ", ";
        }
        console.log(str);
        result.innerHTML = str;
        document.getElementById('merging').innerHTML = time.stop();
    });
};


