var yProfiler = function(){
var nTimeStart = 0,
     nTimeStop  = 0,
     nLastDuration = -1,
     oDate = null;
     
    function fStart(){
        oDate = new Date();
        nTimeStart = oDate.getTime();
        return nTimeStart;

    }

    function fStop(){

        oDate = new Date();
        nTimeStop = oDate.getTime();
        nLastDuration = nTimeStop-nTimeStart;
        return nLastDuration;
    }

    function fTime(){

        // nie wystartowano jeszcze :/

        if (!nTimeStart){
            return false;

        }

        // nie skonczono jeszcze

        if (!nTimeStop){
            fStop();

        }
        if (nLastDuration < 0 || isNaN(nLastDuration)){
            nLastDuration = nTimeStop - nTimeStart;

        }
        return nLastDuration;

    }
    function fGetStart(){

        return (nTimeStart) ? nTimeStart : false;

    }

    function fGetStop() {
        return (nTimeStop) ? nTimeStop : false;

    }
    function fLoop(func, n){
        fStart();

        for(i = 0; i < n; i++){
            func();
        }
        fStop();
        return nLastDuration;
    }
    // konstruktor:
    fStart(); // domyslnie startuje
    return {
        start   : fStart,
        stop    : fStop,
        time    : fTime,
        getStart: fGetStart,
        getStop : fGetStop,
        loop    : fLoop
    };

};